'use strict';

const connection = require('../connection');

/**
 * Initial page rendering
 */
function getTicketEditRoute(req, res) {
    const inHealthCardNumber = req.body.healthCardNumber;
    const inDOB = req.body.DOB;
    const inGender = req.body.Gender;
    const inEmail = req.body.Email;
    const inAddress = req.body.Address;
    const inPhone = req.body.Phone;
    const inAction = req.body.Action;

    console.log(inHealthCardNumber);
    console.log(inDOB);
    console.log(inGender);
    console.log(inEmail);
    console.log(inAddress);
    console.log(inPhone);
    console.log(inAction);



}

function formatDate(date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    console.log(day);
    console.log(month);
    console.log(year);
    return [year, month, day].join('-');
}
/**
 * Form submission
 */
function postTicketEditRoute(req, res, next) {

  // grab data
  const inHealthCardNumber = req.body.healthCardNumber;
  let inDOB = req.body.DOB;
  const inGender = req.body.Gender;
  const inEmail = req.body.Email;
  const inAddress = req.body.Address;
  const inPhone = req.body.Phone;
  const inPatient = req.body.Patient;
  const inAction = req.body.Action;

  if (inAction === 'Edit') {
    // have to formate date for display
    inDOB = formatDate(req.body.DOB);
    console.log('render edit');
    console.log(inHealthCardNumber);
    console.log(inDOB);
    console.log(inGender);
    console.log(inEmail);
    console.log(inAddress);
    console.log(inPhone);
    console.log(inPatient);
    console.log(inAction);

    // render ticket-edit
    res.render('ticket-edit', {
      title: 'MediSquare | Edit Patient Info',
      pageId: 'ticketEdit',
      username: req.session.username,
      message: '',
      HealthCardNumber: inHealthCardNumber,
      DOB: inDOB,
      Gender: inGender,
      Email: inEmail,
      Address: inAddress,
      Phone: inPhone,
      Patient: inPatient,
      Action: inAction,
    }); // end render
  } else {
    console.log('render save');
    console.log(inHealthCardNumber);
    console.log(inDOB);
    console.log(inGender);
    console.log(inEmail);
    console.log(inAddress);
    console.log(inPhone);
    console.log(inPatient);
    console.log(inAction);
    // prepare SQL string
    const updatePatientSQLStr = `UPDATE patients
                                 SET dob='${inDOB}',
                                     gender='${inGender}',
                                     email='${inEmail}',
                                     address='${inAddress}',
                                     phone='${inPhone}'

                                 WHERE health_card_number='${inHealthCardNumber}'`;


    try {
      console.log(updatePatientSQLStr);
      // update ticket detail
      connection.db.query(updatePatientSQLStr, (err2, result2) => {
        if (err2) {
          return res.status(500).send(err2);
        }
        res.redirect(`/admin/ticket-detail?healthCardNumber=${inHealthCardNumber}`);
        return result2;
      }); // end insert
    } catch (error) {
      next();
    }

  }
}


module.exports = {
  get: getTicketEditRoute,
  post: postTicketEditRoute,
};
