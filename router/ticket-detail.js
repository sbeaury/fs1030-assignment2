'use strict';

const connection = require('../connection');


/**
 * Ticket detail page
 */

module.exports = {
  getTicketDetailRoute: (req, res) => {
    const ticketNumber = req.query.ticketNumber;
    const query = `SELECT * FROM tickets WHERE ticket_number='${ticketNumber}'`;
    // execute query
    connection.db.query(query, (err, result) => {
      if (err) {
        res.redirect('/');
      }

      const ticketDept = JSON.parse(JSON.stringify(result))[0].department;
      const ticketDesc =  JSON.parse(JSON.stringify(result))[0].description;
      const ticketStatus =  JSON.parse(JSON.stringify(result))[0].status;

      res.render('ticket-detail.ejs', {
        patients: result,
        title: 'Ticket detail',
        pageId: 'ticket-detail',
        username: req.session.username,
        ticketNumber: ticketNumber,
        ticketDept: ticketDept,
        ticketDesc: ticketDesc,
        ticketStatus: ticketStatus,
      });
    });
  },

};
