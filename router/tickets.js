'use strict';

const connection = require('../connection');

/**
 * Search page
 */

module.exports = {
  getSearchRoute: (req, res) => {
    const query = "SELECT * FROM `tickets` ORDER BY date_open DESC";

    // execute query
    connection.db.query(query, (err, result) => {
      if (err) {
        res.redirect('/');
      }

      res.render('tickets.ejs', {
        tickets: result,
        title: 'Ticketing Management Systems',
        pageId: 'tickets',
        username: req.session.username,
      });
    });
  },
};
