'use strict';

const express = require('express');

const router = express.Router();
const { getHomeRoute } = require('./home');
const { getSearchRoute } = require('./tickets');
const loginRoutes = require('./login');
const logoutRoutes = require('./logout');
const registerRoutes = require('./register');
const { getTicketDetailRoute } = require('./ticket-detail');
const { getCreateTicketRoute, postCreateTicketRoute } = require('./create-ticket');
const ticketEditRoutes = require('./ticket-edit');
const { postCloseTicketRoute } = require('./close-ticket');


/**
 * Define routes
 */

// Home page
router.get('/', getHomeRoute);

// Search page
router.get('/admin/tickets', getSearchRoute);

// Ticket detail page
router.get('/admin/ticket-detail', getTicketDetailRoute);

// Create ticket page
router.get('/admin/create', getCreateTicketRoute);
router.post('/admin/create', postCreateTicketRoute);

// Ticket edit page
router.get('/admin/ticket-edit', ticketEditRoutes.get);
router.post('/admin/ticket-edit', ticketEditRoutes.post);

// Close ticket page
router.post('/admin/close-ticket', postCloseTicketRoute);

// Login page
router.get('/login', loginRoutes.get);
router.post('/login', loginRoutes.post);

// Logout
router.get('/logout', logoutRoutes.get);

// Register page
router.get('/register', registerRoutes.get);
router.post('/register', registerRoutes.post);



module.exports = router;
