'use strict';

const connection = require('../connection');

module.exports = {
  getCreateTicketRoute: (req, res) => {
    res.render('create-ticket', {
      title: 'Ticket Management System | Create Ticket',
      pageId: 'create',
      username: req.session.username,
      message: '',
    });
  },

  postCreateTicketRoute: (req, res, next) => {

    // grab form data

    const inTicketNumber = req.body.ticketNumber;
    const inTicketDept = req.body.ticketDept;
    const inTicketDesc = req.body.ticketDesc;

    // check form
    const formErrors = {
      ticketNumber: inTicketNumber  ? null : 'Invalid Ticket Number',
      ticketDept: inTicketDept  ? null : 'Invalid Ticket Dept',
      ticketDesc: inTicketDesc  ? null : 'Invalid Ticket Desc',
    };// end formErrors

    console.log(formErrors);
    // If there are any errors do not create patient
    // validate form data
    if (formErrors.ticketNumber) {
      console.log('invalid form');
      console.log(formErrors);
    } else {
      // create patient
      console.log('valid form');
      const healthCardNumberQuery = `SELECT *
                                     FROM tickets
                                     WHERE ticket_number = '${String(inTicketNumber)}'`;

      // get health card
      connection.db.query(healthCardNumberQuery, (err, result) => {
        if (err) {
          return res.status(500).send(err);
        }
        if (result.length > 0) {
          const message = 'Ticket Number already exists';
          res.render('create-ticket.ejs', {
            message,
            title: 'Ticket Management System | Create ticket',
            pageId: 'create',
            username: req.session.username,
          });
        } else {
          // create new patient
          const query = `INSERT INTO tickets (ticket_number, department, description) VALUES (
                           '${inTicketNumber}',
                           '${inTicketDept}',
                           '${inTicketDesc}'
                           );
                           `;
          console.log(query);
          connection.db.query(query, (err2, result2) => {
            if (err2) {
              return res.status(500).send(err2);
            }
            res.redirect('/admin/tickets');
            return result2;
          }); // end insert
        }
      });
    }
  }, // end postCreateTicketRoute
}; // end module export
