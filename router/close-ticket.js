'use strict';

const connection = require('../connection');


/**
 * Ticket detail page
 */

module.exports = {
  postCloseTicketRoute: (req, res) => {
    const ticketNumber = req.query.ticketNumber;

    function formatDate(date) {
      let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [year, month, day].join('-');
    }


    const currentDate = formatDate(Date.now());
    const currentTime = new Date(Date.now()).toLocaleTimeString('en-US', { hour12: false });
    const currentDateTime = currentDate + ' ' + currentTime;
    if (ticketNumber) {
      const query = `UPDATE tickets SET status='Closed', date_closed='${currentDateTime}' WHERE ticket_number='${ticketNumber}'`;
      // execute query
      connection.db.query(query, (err, result) => {
        if (err) {
          return res.status(500).send(err);
        }
        res.redirect(`/admin/ticket-detail?ticketNumber=${ticketNumber}`);
        return result;
      }); // end insert
    }
  },
};
