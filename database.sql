CREATE DATABASE IF NOT EXISTS fs1030_assignment2_q2;
USE fs1030_assignment2_q2;

-- create ticket table
CREATE TABLE IF NOT EXISTS `tickets` (
 -- `id` int(5) NOT NULL AUTO_INCREMENT,
  `ticket_number` varchar(12) NOT NULL,
  `department` mediumtext NOT NULL,
  `description` longtext NOT NULL,
	`date_open` datetime NOT NULL,
	`date_closed` datetime,
  `status` varchar(6) NOT NULL DEFAULT 'Open',
  PRIMARY KEY (`ticket_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

insert into tickets (ticket_number, department, description, date_open) values ('001-002-9999', 'Procurement', 'Wifi not working', '2015-03-25 12:00:00');
insert into tickets (ticket_number, department, description, date_open) values ('001-002-9998', 'Production', 'New SAP account setup', '2018-03-25 12:00:00');
insert into tickets (ticket_number, department, description, date_open) values ('001-002-9997', 'Procurement', 'Wifi not working', '2019-03-25 12:00:00');

-- create users table for login
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

