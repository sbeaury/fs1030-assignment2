# FS1030 - Assignment 2
## Ticketing Management System

### Getting started

### `npm install`

### Database setup

Run the script file named  ``database.sql`` in MySQL to set up the database environment.

Update environment variables stored in the file named ``.envexample`` (DB_USERNAME, DB_PASSWORD) to link your local MySQL environment to Node.js. Rename it to ``.env ``

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


